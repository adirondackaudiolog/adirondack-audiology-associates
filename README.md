For over 30 years, Adirondack Audiology Associates has been helping improve the quality of life for our patients in Colchester, Plattsburgh, Saranac Lake, Potsdam and the surrounding areas of Vermont and New York.

Address: 6439 State Highway 56, Potsdam, NY 13676

Phone: 315-276-3484